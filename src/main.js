import './main.scss';

/**
 * @class Countdown
 * @description Takes a container element and renders the countdown into it
 */
class Countdown {
  /**
   * Constructor
   * @param {DOMObject} container
   * @param {number} secondsToZero
   */
  constructor(container, secondsToZero) {
    this.POSITIONS = ['units', 'tens'];
    this.NUMBERS = [
      '<svg viewBox="0 0 15 20"><path d="m10 15.604q0 1.1813-0.37855 1.9368-0.37855 0.75549-1.041 1.3049-0.66246 0.54945-1.5931 0.85165t-1.9716 0.3022q-1.041 0-1.9874-0.3022-0.94637-0.3022-1.6088-0.85165-0.66246-0.54945-1.041-1.3049-0.37855-0.7555-0.37855-1.9368v-11.209q0-1.1813 0.37855-1.9368 0.37855-0.75549 1.041-1.3049 0.66246-0.54945 1.6088-0.85165 0.94637-0.3022 1.9874-0.3022 1.041 0 1.9716 0.3022t1.5931 0.85165q0.66246 0.54945 1.041 1.3049t0.37855 1.9368zm-3.3438-11.319q0-0.85165-0.47319-1.2637t-1.1672-0.41209-1.183 0.41209-0.48896 1.2637v11.429q0 0.85165 0.48896 1.2637t1.183 0.41209 1.1672-0.41209 0.47319-1.2637z" stroke-width=".91996"/></svg>',
      '<svg viewBox="0 0 15 20"><path d="m4.9828 20v-16.517l-3.424 2.3034v-3.3708l3.424-2.4157h3.392v20z" stroke-width=".93692"/></svg>',
      '<svg viewBox="0 0 15 20"><path d="m0.094937 20v-2.5l5.6329-9.6111q0.37975-0.63889 0.64873-1.5417 0.26899-0.90278 0.26899-1.9861 0-0.86111-0.45886-1.2917-0.45886-0.43056-1.1551-0.43056-0.6962 0-1.1867 0.41667-0.49051 0.41667-0.49051 1.2778v1.2778h-3.3544v-1.1667q0-1.1944 0.36392-1.9583t1.0285-1.3194q0.66456-0.55556 1.6456-0.86111 0.98101-0.30556 1.9937-0.30556 1.0127 0 1.8987 0.25 0.88608 0.25 1.5823 0.80556 0.66456 0.55556 1.0759 1.3889 0.41139 0.83333 0.41139 2.0278 0 1.0833-0.3481 2.25-0.3481 1.1667-0.88608 2.0833l-5.0949 8.5556h6.3291v2.6389z" stroke-width=".92652"/></svg>',
      '<svg viewBox="0 0 15 20"><path d="m10 14.203q0 0.7967-0.03125 1.4423-0.03125 0.6456-0.17188 1.2225t-0.4375 1.0714q-0.29688 0.49451-0.82812 0.93407-0.65625 0.54945-1.5938 0.83791-0.9375 0.28846-1.9688 0.28846t-1.9844-0.3022-1.6094-0.85165q-0.65625-0.54945-1.0156-1.3049-0.35938-0.7555-0.35938-1.9368v-1.1538h3.3125v1.2637q0 0.85165 0.48438 1.2637t1.1719 0.41209q0.40625 0 0.70312-0.13736 0.29688-0.13736 0.48438-0.35714 0.34375-0.38462 0.4375-1.1813 0.09375-0.7967 0.09375-1.7033 0-0.98901-0.078125-1.717t-0.42188-1.1401q-0.4375-0.4945-1.25-0.4945h-1.0938v-2.4451h1.0938q0.6875 0 1.0625-0.38462t0.4375-1.044q0.0625-0.65934 0.0625-1.4286 0-0.74176-0.0625-1.3462t-0.375-0.96154q-0.40625-0.43956-1.0938-0.43956-0.625 0-1.0625 0.38462t-0.4375 1.1813v1.3736h-3.3125v-1.1813q0-1.1538 0.35938-1.9231 0.35938-0.76923 1.0156-1.3187 1.3438-1.1264 3.4375-1.1264 1.0938 0 1.9531 0.28846t1.4531 0.81044q0.75 0.65934 1.0938 1.6346t0.34375 2.4313q0 0.98901-0.09375 1.7308t-0.46875 1.3736q-0.4375 0.71429-1.2188 1.1538 0.40625 0.24725 0.73438 0.53571 0.32812 0.28846 0.54688 0.6456 0.4375 0.71428 0.5625 1.5247 0.125 0.81044 0.125 2.0742z" stroke-width=".91564"/></svg>',
      '<svg viewBox="0 0 15 20"><path d="m8.6119 16.882v3.118h-3.0028v-3.118h-5.6091v-2.6685l5.1841-14.213h2.9178l-5.0142 14.213h2.5212v-3.5674h3.0028v3.5674h1.3881v2.6685z" stroke-width=".88153"/></svg>',
      '<svg viewBox="0 0 15 20"><path d="m10 13.111q0 0.97222-0.03125 1.8056-0.03125 0.83333-0.17188 1.5278-0.14062 0.69444-0.4375 1.2639-0.29688 0.56944-0.85938 1.0694-0.65625 0.58333-1.5781 0.90278-0.92188 0.31944-1.9531 0.31944t-1.9688-0.29167q-0.9375-0.29167-1.5938-0.84722-0.65625-0.55556-1.0312-1.3333t-0.375-1.9722v-0.97222h3.3125v1.3611q0 0.88889 0.46875 1.3056t1.1875 0.41667q0.78125 0 1.2188-0.52778 0.375-0.44444 0.4375-1.4722 0.0625-1.0278 0.0625-2.2778t-0.046875-2.1389-0.39062-1.3611q-0.40625-0.61111-1.2812-0.61111-0.71875 0-1.1875 0.44444-0.46875 0.44444-0.46875 1.25v0.41667h-3.1562v-11.389h9.5938v2.6389h-6.5625v5.3056q0.25-0.27778 0.53125-0.52778t0.625-0.41667q0.75-0.36111 1.5938-0.36111 1.0312 0 1.75 0.29167 0.71875 0.29167 1.25 0.90278 0.71875 0.77778 0.89062 2.1111 0.17188 1.3333 0.17188 3.1667z" stroke-width=".92071"/></svg>',
      '<svg viewBox="0 0 15 20"><path d="m10 13.778q0 1-0.062305 1.7083t-0.20249 1.25-0.3271 0.94444q-0.18692 0.40278-0.46729 0.73611-0.65421 0.83333-1.6978 1.2083-1.0436 0.375-2.2586 0.375-1.3396 0-2.3053-0.41667-0.96573-0.41667-1.5576-1.1667-0.34268-0.41667-0.54517-0.86111-0.20249-0.44444-0.34268-0.98611-0.14019-0.54167-0.18692-1.2222-0.046729-0.68056-0.046729-1.5694 0-1.3333 0.24922-2.5139 0.24922-1.1806 0.65421-2.1528l3.8941-9.1111h3.4268l-3.6449 8q0.28037-0.19444 0.65421-0.26389 0.37383-0.069444 0.71651-0.069444 0.90343 0 1.6511 0.29167 0.74766 0.29167 1.2773 0.93056 0.31153 0.33333 0.51402 0.73611 0.20249 0.40278 0.34268 0.95833 0.14019 0.55556 0.20249 1.3333 0.062305 0.77778 0.062305 1.8611zm-3.3022-0.027778v-0.91667t-0.046729-0.88889q-0.046729-0.88889-0.15576-0.77778-0.10903 0.11111-0.29595-0.58333-0.43614-0.5-1.1838-0.5-0.34268 0-0.66978 0.125t-0.54517 0.375q-0.18692 0.22222-0.29595 0.58333t-0.15576 0.77778q-0.046729 0.41667-0.046729 0.88889v0.91667q0 0.88889 0.046729 1.7639t0.45171 1.3472q0.21807 0.25 0.54517 0.375t0.66978 0.125q0.74766 0 1.1838-0.5 0.40498-0.47222 0.45171-1.3472 0.046729-0.875 0.046729-1.7639z" stroke-width=".91928"/></svg>',
      '<svg viewBox="0 0 15 20"><path d="m4.3675 20h-3.1928l5.6325-17.331h-3.7349v2.6685h-3.0723v-5.3371h10v2.4719z" stroke-width=".90898"/></svg>',
      '<svg viewBox="0 0 15 20"><path d="m10 14.258q0 0.7967-0.046729 1.456-0.046729 0.65934-0.20249 1.2088-0.15576 0.54945-0.43614 1.0165-0.28037 0.46703-0.77882 0.90659-0.65421 0.57692-1.5888 0.86538-0.93458 0.28846-1.9315 0.28846-0.99688 0-1.947-0.28846-0.95016-0.28846-1.6044-0.86538-0.49844-0.43956-0.77882-0.90659-0.28037-0.46703-0.43614-1.0165-0.15576-0.54945-0.20249-1.2088-0.046729-0.65934-0.046729-1.456 0-1.2363 0.12461-2.033 0.12461-0.7967 0.5296-1.4286 0.21807-0.38462 0.54517-0.67308 0.3271-0.28846 0.76324-0.56319-0.77882-0.49451-1.215-1.2088-0.37383-0.63187-0.48287-1.3324t-0.10903-1.717q0-1.4286 0.29595-2.4313 0.29595-1.0027 1.1371-1.7445 0.65421-0.57692 1.5265-0.85165 0.87227-0.27473 1.8692-0.27473 0.99688 0 1.8847 0.27473 0.88785 0.27473 1.5421 0.85165 0.84112 0.74176 1.1371 1.7445 0.29595 1.0027 0.29595 2.4313 0 1.0165-0.10903 1.717t-0.48287 1.3324q-0.43614 0.71429-1.215 1.2088 0.43614 0.27473 0.74766 0.56319 0.31153 0.28846 0.56075 0.67308 0.40498 0.63187 0.5296 1.4286 0.12461 0.7967 0.12461 2.033zm-3.3022-0.13736q0-0.93407-0.046729-1.6896-0.046729-0.7555-0.45171-1.1951-0.43614-0.46703-1.1838-0.46703-0.77882 0-1.215 0.46703-0.40498 0.43956-0.45171 1.1951-0.046729 0.75549-0.046729 1.6896 0 0.85165 0.062305 1.6071 0.062305 0.75549 0.43614 1.1951 0.43614 0.46703 1.215 0.46703 0.74766 0 1.1838-0.46703 0.37383-0.41209 0.43614-1.1813 0.062305-0.76923 0.062306-1.6209zm-0.15576-8.6813q0-0.71429-0.062305-1.3874-0.062306-0.67308-0.40498-1.0577-0.34268-0.38462-1.0592-0.38462-0.34268 0-0.62305 0.082418-0.28037 0.082418-0.46729 0.3022-0.34268 0.38462-0.40498 1.0577-0.062305 0.67308-0.062305 1.3874 0 0.71429 0.062305 1.4423t0.49844 1.1126q0.37383 0.32967 0.99688 0.32967 0.62305 0 0.99688-0.32967 0.43614-0.38462 0.48287-1.1126t0.046729-1.4423z" stroke-width=".91421"/></svg>',
      '<svg viewBox="0 0 15 20"><path d="m10 6.2222q0 1.3333-0.24922 2.5139-0.24922 1.1806-0.65421 2.1528l-3.8941 9.1111h-3.4268l3.6449-8q-0.28037 0.19444-0.63863 0.26389-0.35826 0.06944-0.70093 0.06944-0.90343 0-1.6667-0.29167-0.76324-0.29166-1.2928-0.93055-0.31153-0.33333-0.51402-0.73611-0.20249-0.40278-0.34268-0.95833-0.14019-0.55556-0.20249-1.3333-0.062305-0.77778-0.062305-1.8611 0-1 0.062305-1.7083 0.062305-0.70833 0.20249-1.25 0.14019-0.54167 0.34268-0.94444 0.20249-0.40278 0.48287-0.73611 0.65421-0.83333 1.6822-1.2083t2.243-0.375q1.3396 0 2.3209 0.41667 0.98131 0.41667 1.5732 1.1667 0.31153 0.41667 0.5296 0.86111 0.21807 0.44444 0.34268 0.98611 0.12461 0.54167 0.17134 1.2222 0.046729 0.68056 0.046729 1.5694zm-3.3022 0.027778q0-0.88889-0.046729-1.7639t-0.45171-1.3472q-0.43614-0.5-1.1838-0.5-0.80997 0-1.215 0.5-0.40498 0.47222-0.45171 1.3472-0.046729 0.875-0.046729 1.7639v0.91667t0.046729 0.88889q0.046729 0.88889 0.15576 0.77778 0.10903-0.11111 0.29595 0.58333 0.21807 0.25 0.54517 0.375t0.66978 0.125q0.74766 0 1.1838-0.5 0.18692-0.22222 0.29595-0.58333t0.15576-0.77778q0.046729-0.41667 0.046729-0.88889z" stroke-width=".91928"/></svg>'
    ];
    this.SEPARATOR = `<svg viewBox="0 0 15 20">
      <g stroke-width="8">
        <circle cx="7.5" cy="8" r="1" style="paint-order:stroke markers fill"/>
        <circle cx="7.5" cy="13" r="1" style="paint-order:stroke markers fill"/>
      </g>
    </svg>`;

    this.container = container;
    this.secondsToZero = secondsToZero;
    this.animationDuration = 500;
    this.parts = [
      {type: 'days', calculateValue: position => this.getDigit(position, Math.floor(this.secondsToZero / 60 / 60 / 24))},
      {type: 'hours', calculateValue: position => this.getDigit(position, Math.floor((this.secondsToZero / 60 / 60)) % 24)},
      {type: 'minutes', calculateValue: position => this.getDigit(position, Math.floor((this.secondsToZero / 60)) % 60)},
      {type: 'seconds', calculateValue: position => this.getDigit(position, this.secondsToZero % 60)}
    ];

    this.countdownElement = document.createElement('div');
    this.countdownElement.classList.add('ae-countdown');
    this.countdownElement.style.visibility = 'hidden';

    this.container.appendChild(this.countdownElement);

    for (let partIndex = 0; partIndex < this.parts.length; partIndex++) {
      const part = this.parts[partIndex];

      part.container = document.createElement('div');
      part.container.classList.add('part');
      part.container.classList.add(`part--${part.type}`);

      part.digitContainer = document.createElement('div');
      part.digitContainer.classList.add('part__digits');
      part.units = this.getDigitElementSimple(part, 'units');
      part.tens = this.getDigitElementSimple(part, 'tens');

      part.container.appendChild(part.digitContainer);
      part.digitContainer.appendChild(part.tens.digit);
      part.digitContainer.appendChild(part.units.digit);

      this.countdownElement.appendChild(part.container);

      if (partIndex < this.parts.length - 1) {
        const separator = document.createElement('div');
        separator.innerHTML = this.SEPARATOR;
        separator.classList.add('separator');
        this.countdownElement.appendChild(separator);
      }
    }
  }

  /**
   * Return either the tens or units of a given number
   * @param {string} position
   * @param {number} number
   * @returns {number}
   */
  getDigit(position, number) {
    return position === 'units' ? number % 10 : Math.floor(number / 10);
  }

  /**
   * Returns the DOMObject for the digit of the given part and position
   * @param {Object} part
   * @param {string} position
   * @returns {Object}
   */
  getDigitElementSimple(part, position) {
    const digit = document.createElement('span');
    digit.classList.add(`${part.type}__${position}`);
    digit.classList.add('digit');

    return {
      digit
    };
  }

  /**
   * Update the given DOMObject of the part and position with the given number
   * @param {DOMObject} partPosition
   * @param {number} value
   */
  updateUi(partPosition, value) {
    partPosition.digit.innerHTML = this.NUMBERS[value];
  }

  /**
   * Handles everything for a second passing
   */
  processSecond() {
    this.POSITIONS.forEach(position => {
      this.parts.forEach(part => {
        if (
          (this.secondsToZero < 86400 && part.type === 'days') ||
          (this.secondsToZero < 3600 && part.type === 'hours') ||
          (this.secondsToZero < 60 && part.type === 'minutes') ||
          (this.secondsToZero === 0 && part.type === 'seconds')
        ) {
          part.container.classList.add('part--zero');
        }

        const partPosition = part[position];
        const newValue = part.calculateValue(position);

        if (partPosition.previousValue !== newValue) {
          partPosition.previousValue = newValue;
          this.updateUi(partPosition, newValue);
        }
      });
    });
  }

  /**
   * Recursive loop for processing the decreasing seconds
   */
  countdown() {
    window.setTimeout(() => {
      this.processSecond();

      window.setTimeout(() => {
        this.countdownElement.style.visibility = 'visible';
      }, 500);

      this.secondsToZero--;
      this.secondsToZero >= 0 && this.countdown();
    }, 1000);
  }
};

/**
 * @description Renders a countdown
 * @param {DOMObject} container - Container to render the countdown in
 */
export default (container) => {
  const date = new Date(container.getAttribute('data-countdown-to'));
  const diff = date - new Date();
  const seconds = Math.ceil(diff / 1000) + (date.getTimezoneOffset() * -60);
  const countdown = new Countdown(container, seconds > 0 ? seconds : 0);
  countdown.countdown();
};
